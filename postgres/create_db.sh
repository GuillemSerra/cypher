rm -f postgres/init.sql

cat cypher/users/infra/sql/user.sql >> postgres/init.sql
cat cypher/stories/infra/sql/stories.sql >> postgres/init.sql
cat cypher/stories/infra/sql/reactions.sql >> postgres/init.sql

cat cypher/relations.sql >> postgres/init.sql
