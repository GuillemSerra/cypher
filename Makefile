.PHONY: migrate up full-up down

migrate:
	docker-compose -f docker-compose.yml down -v
	sh postgres/create_db.sh
	docker-compose -f docker-compose.yml up --build postgres

up:
	docker-compose -f docker-compose.yml up --build

full-up:
	docker-compose -f docker-compose.yml -f docker-compose.monitoring.yml up --build

down:
	docker-compose -f docker-compose.yml -f docker-compose.monitoring.yml down
