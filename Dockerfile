FROM python:3.8-slim-buster

ENV PYTHONUNBUFFERED=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    POETRY_VERSION=1.0.2

RUN apt-get update && apt-get install -y gcc

# deps
RUN pip install "poetry==$POETRY_VERSION"

# Copy poetry files
WORKDIR /deps
COPY poetry.lock pyproject.toml /deps/

RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi

# Copy project code
WORKDIR /app
COPY cypher/ .

CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--loop",  "uvloop"]
