from starlette.applications import Starlette
from starlette.routing import Mount, Route

from starlette_prometheus import metrics, PrometheusMiddleware

import config
import db
import users.routes as user_routes
import stories.routes as stories_routes


def add_state() -> None:
    app.state.db_pool = db.DB_POOL


def add_middlewares() -> None:
    app.add_middleware(PrometheusMiddleware)


app: Starlette = Starlette(
    debug=config.DEBUG,
    on_startup=[db.init_db_pool, add_state, add_middlewares],
    on_shutdown=[db.shutdown_db_pool],
    routes=[
        Mount("/v1/users", routes=user_routes.routes),
        Mount("/v1/stories", routes=stories_routes.routes),
        Route("/metrics/", metrics)
    ]
)
