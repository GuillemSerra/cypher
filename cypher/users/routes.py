from starlette.routing import Route

from users import views


routes = [
    Route('/{user_id}', views.get_user, methods=['GET']),
    Route('/', views.create_user, methods=['POST']),
]
