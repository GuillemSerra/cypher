from dataclasses import dataclass

from datetime import datetime


@dataclass
class User:

    id: int
    name: str
    date_joined: datetime

    @classmethod
    def from_dict(cls, data: dict) -> 'User':
        return cls(
            id=data['id'],
            name=data['name'],
            date_joined=data['date_joined'].strftime("%d-%m-%Y, %H:%M:%S"),
        )
