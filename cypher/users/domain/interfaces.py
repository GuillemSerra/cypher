import abc

from users import requests
from users.domain import entities


class UserRepository(abc.ABC):

    @abc.abstractmethod
    async def get(self, user_id: int) -> entities.User:
        pass

    @abc.abstractmethod
    async def create(self, data: requests.CreateUser) -> entities.User:
        pass
