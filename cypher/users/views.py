from starlette.responses import UJSONResponse
from starlette.requests import Request
from pydantic import ValidationError

from users import requests
from users.infra import repositories


async def get_user(request: Request) -> UJSONResponse:
    user_id = int(request.path_params['user_id'])

    repo = repositories.User(request.app.state.db_pool)
    user = await repo.get(user_id)

    return UJSONResponse(
        status_code=200,
        content=user
    )


async def create_user(request: Request) -> UJSONResponse:
    try:
        user_data = requests.CreateUser(**(await request.json()))
    except ValidationError as errors:
        return UJSONResponse(
            status_code=400,
            content=errors
        )

    repo = repositories.User(request.app.state.db_pool)
    user = await repo.create(user_data)

    return UJSONResponse(
        status_code=201,
        content=user
    )
