from datetime import datetime

import asyncpg

from users import requests
from users.domain import entities, interfaces


class User(interfaces.UserRepository):

    __tablename__ = 'users_users'

    def __init__(self, db_pool: asyncpg.pool.Pool) -> None:
        self.db_pool = db_pool

    async def get(self, user_id: int) -> entities.User:
        query = f"SELECT * FROM {self.__tablename__} WHERE id = $1"

        async with self.db_pool.acquire() as conn:
            user = await conn.fetchrow(query, user_id)

        return entities.User.from_dict(user)

    async def create(self, user_data: requests.CreateUser) -> entities.User:
        query = f"INSERT INTO {self.__tablename__} (name, date_joined) VALUES ($1, $2) RETURNING id, name, date_joined"

        async with self.db_pool.acquire() as conn:
            user = await conn.fetchrow(query, user_data.name, datetime.now())

        return entities.User.from_dict(user)
