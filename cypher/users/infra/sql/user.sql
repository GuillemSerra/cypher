CREATE TABLE users_users (
    id           serial PRIMARY KEY,
    name         varchar(40) NOT NULL,
    date_joined  timestamp NOT NULL
);
