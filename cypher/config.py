from starlette.config import Config
from starlette.datastructures import Secret


config = Config()

DEBUG = config('DEBUG', cast=bool, default=False)

DB_HOST = config('DB_HOST', cast=Secret)
DB_PSWD = config('DB_PSWD', cast=Secret)
DB_USER = config('DB_USER', cast=Secret)
DB_NAME = config('DB_NAME', cast=Secret)
