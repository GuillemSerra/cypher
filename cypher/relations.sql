ALTER TABLE stories_stories ADD CONSTRAINT fk_stories_owners FOREIGN KEY (owner_id) REFERENCES users_users (id);
ALTER TABLE stories_reactions ADD CONSTRAINT fk_reactions_stories FOREIGN KEY (story_id) REFERENCES stories_stories (id);
ALTER TABLE stories_reactions ADD CONSTRAINT fk_reactions_users FOREIGN KEY (user_id) REFERENCES users_users (id);

