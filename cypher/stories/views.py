from starlette.requests import Request
from starlette.responses import UJSONResponse

from pydantic import ValidationError

from stories import requests
from stories.domain import use_cases
from stories.infra import repositories


async def create_story(request: Request) -> UJSONResponse:
    try:
        story_data = requests.CreateStory(**(await request.json()))
    except ValidationError as errors:
        return UJSONResponse(
            status_code=400,
            content=errors
        )

    repo = repositories.Stories(request.app.state.db_pool)
    story = await repo.create(story_data)

    return UJSONResponse(
        status_code=201,
        content=story
    )


async def get_story(request: Request) -> UJSONResponse:
    story_data = requests.GetStory(story_id=int(request.path_params['story_id']))

    use_case = use_cases.GetStory(
        repositories.Stories(request.app.state.db_pool),
        repositories.Reactions(request.app.state.db_pool)
    )
    story = await use_case.execute(story_data)

    return UJSONResponse(
        status_code=200,
        content=story
    )


async def add_reaction(request: Request) -> UJSONResponse:
    try:
        reaction_data = await request.json()
        reaction_data['story_id'] = int(request.path_params['story_id'])
        reaction_data = requests.AddReaction(**reaction_data)
    except ValidationError as errors:
        return UJSONResponse(
            status_code=400,
            content=errors
        )

    repo = repositories.Reactions(request.app.state.db_pool)
    reaction = await repo.add(reaction_data)

    return UJSONResponse(
        status_code=201,
        content=reaction
    )
