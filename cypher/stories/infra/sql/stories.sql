CREATE TABLE stories_stories (
    id                   serial PRIMARY KEY,
    owner_id             int NOT NULL,
    content              varchar(240) NOT NULL,
    place                varchar(128) NOT NULL,
    coordinates          point NOT NULL,
    when_date            timestamp NOT NULL,
    creation_date        timestamp NOT NULL
);

