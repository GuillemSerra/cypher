CREATE TABLE stories_reactions (
    story_id int NOT NULL,
    user_id  int NOT NULL,
    reaction text NOT NULL,

    PRIMARY KEY (story_id, user_id)
);
