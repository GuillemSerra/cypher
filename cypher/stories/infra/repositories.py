from datetime import datetime
from typing import List, Tuple

import asyncpg

from stories import requests
from stories.domain import entities, interfaces


class Stories(interfaces.StoriesRepository):

    __tablename__ = 'stories_stories'

    def __init__(self, db_pool: asyncpg.pool.Pool) -> None:
        self.db_pool = db_pool

    async def create(self, data: requests.CreateStory) -> entities.Story:
        query = f"INSERT INTO {self.__tablename__} (owner_id, content, place, coordinates, when_date, creation_date) \
                  VALUES ($1, $2, $3, $4, $5, $6) RETURNING id, owner_id, content, place, coordinates, when_date, creation_date"

        async with self.db_pool.acquire() as conn:
            story = await conn.fetchrow(
                query, data.owner_id, data.content, data.place, data.coordinates, datetime.now(), datetime.now()
            )

        return entities.Story.from_dict(story)

    async def get(self, story_id: int) -> entities.Story:
        query = f"SELECT * from {self.__tablename__} WHERE id = $1"

        async with self.db_pool.acquire() as conn:
            story = await conn.fetchrow(query, story_id)

        return entities.Story.from_dict(story)


class Reactions(interfaces.ReactionsRepository):

    __tablename__ = 'stories_reactions'

    def __init__(self, db_pool: asyncpg.pool.Pool) -> None:
        self.db_pool = db_pool

    async def add(self, data: requests.AddReaction) -> entities.Reaction:
        query = f"INSERT INTO {self.__tablename__} (story_id, user_id, reaction) \
                  VALUES ($1, $2, $3) RETURNING story_id, user_id, reaction"

        async with self.db_pool.acquire() as conn:
            reaction = await conn.fetchrow(
                query, data.story_id, data.user_id, data.reaction
            )

        return entities.Reaction.from_dict(reaction)

    async def get_counters_from_story(self, story_id: int) -> List[Tuple[str, int]]:
        query = f"SELECT reaction, count(*) FROM {self.__tablename__} WHERE story_id = $1 \
                  GROUP BY reaction ORDER BY 2 DESC"

        async with self.db_pool.acquire() as conn:
            counters = await conn.fetch(query, story_id)

        return [(row['reaction'], row['count']) for row in counters]
