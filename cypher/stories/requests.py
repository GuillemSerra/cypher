from typing import Tuple

from pydantic import BaseModel


class CreateStory(BaseModel):

    owner_id: int
    content: str
    place: str
    coordinates: Tuple[int, int]


class GetStory(BaseModel):

    story_id: int


class AddReaction(BaseModel):

    story_id: int
    user_id: int
    reaction: str
