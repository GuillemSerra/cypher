from starlette.routing import Route

from stories import views


routes = [
    Route('/{story_id}/reactions', views.add_reaction, methods=['POST']),
    Route('/{story_id}', views.get_story, methods=['GET']),
    Route('/', views.create_story, methods=['POST']),
]
