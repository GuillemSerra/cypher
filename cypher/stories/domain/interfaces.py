import abc

from typing import List, Tuple

from stories import requests
from stories.domain import entities


class StoriesRepository(abc.ABC):

    @abc.abstractmethod
    async def create(self, data: requests.CreateStory) -> entities.Story:
        pass

    @abc.abstractmethod
    async def get(self, story_id: int) -> entities.Story:
        pass


class ReactionsRepository(abc.ABC):

    @abc.abstractmethod
    async def add(self, data: requests.AddReaction) -> entities.Reaction:
        pass

    @abc.abstractmethod
    async def get_counters_from_story(self, story_id: int) -> List[Tuple[str, int]]:
        pass
