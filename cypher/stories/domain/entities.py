from dataclasses import dataclass
from datetime import datetime
from typing import List, Tuple


@dataclass
class Story:

    id: int
    owner_id: int
    content: str
    reactions: List[Tuple[str, int]]
    place: str
    coordinates: Tuple[int, int]
    when_date: datetime
    creation_date: datetime

    @classmethod
    def from_dict(cls, data: dict) -> 'Story':
        return cls(
            id=data['id'],
            owner_id=data['owner_id'],
            content=data['content'],
            reactions=data.get('reactions', list()),
            place=data['place'],
            coordinates=data['coordinates'],
            when_date=data['when_date'].strftime("%d-%m-%Y, %H:%M:%S"),
            creation_date=data['creation_date'].strftime("%d-%m-%Y, %H:%M:%S"),
        )


@dataclass
class Reaction:

    story_id: int
    user_id: int
    reaction: str

    @classmethod
    def from_dict(cls, data: dict) -> 'Reaction':
        return cls(
            story_id=data['story_id'],
            user_id=data['user_id'],
            reaction=data['reaction'],
        )
