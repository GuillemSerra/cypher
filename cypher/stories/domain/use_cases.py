import asyncio

from stories import requests
from stories.domain import entities, interfaces


class GetStory:

    def __init__(self, stories_repo: interfaces.StoriesRepository, reactions_repo: interfaces.ReactionsRepository):
        self.stories_repo = stories_repo
        self.reactions_repo = reactions_repo

    async def execute(self, data: requests.GetStory) -> entities.Story:
        story, reactions = await asyncio.gather(
            self.stories_repo.get(data.story_id),
            self.reactions_repo.get_counters_from_story(data.story_id)
        )
        story.reactions = reactions

        return story
