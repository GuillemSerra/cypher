import asyncpg

import config


DB_POOL: asyncpg.pool.Pool = None


async def init_db_pool() -> asyncpg.pool.Pool:
    global DB_POOL
    DB_POOL = await asyncpg.create_pool(
        dsn=f"postgres://{str(config.DB_USER)}:{str(config.DB_PSWD)}@{str(config.DB_HOST)}:5432/{str(config.DB_NAME)}",
        min_size=5,
        max_size=50
    )


async def shutdown_db_pool() -> None:
    await DB_POOL.close()
